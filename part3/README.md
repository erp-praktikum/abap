# Kapitel 2: Einführung in das ABAP Dictionary - Verwendung von globalen Strukturen für Datenobjekte

Am Ende dieser Übungen können Sie:

- technische Namen von Datentypen und –Objekten ermitteln, die in einer Anwendung verwendet werden
- Verwendungsnachweise innerhalb einer ABAP-Anwendung durchführen

[Lösung](solution/solution.md)

Eine gegebene ABAP-Anwendung soll dahingehend untersucht werden, ob und wenn ja welche globalen Datentypen verwendet werden.  
Anwendung: `ZABAP010_##_GET_START_1`

1. Legen Sie die Klasse `ZABAP010_##_GET_START_1` für eine Konsolenanwendung an (##: Benutzernummer oder beachten Sie die Namenskonventionen, die Ihnen der Dozent vorgibt).

2. Fügen Sie folgenden Quelltext für die Anwendung ein:

        DATA lv_customer_id TYPE /dmo/customer_id.
        DATA ls_customer    TYPE /dmo/customer.
        
        lv_customer_id = '000003'.
        
        SELECT SINGLE * FROM /dmo/customer
        WHERE customer_id = @lv_customer_id
        INTO @ls_customer.
        
        out->write( EXPORTING data = ls_customer ).

3. Aktivieren Sie die Anwendung.

4. Führen Sie die Anwendung aus.

5. Untersuchen Sie die Anwendung:
    
    1. Welche Datenobjekte gibt es?      
      Wo sind sie definiert?      
      Wo werden Sie verwendet? Nutzen Sie den Verwendungsnachweis (Rechte Maustaste -> Get Where-used List…).
    
    2. Wie erfolgt die Ausgabe in der Konsole?

    3. Über welche Anweisung findet die Kommunikation mit der Datenbank statt?  
      Navigieren Sie in die Schlüsselwort-Dokumentation zu dieser Anweisung (F1).  
      Von welcher Datenbanktabelle werden Daten gelesen?  
      Navigieren Sie in die Definition der Datenbanktabelle. Welche Spalten besitzt die Datenbanktabelle?

    4. Es wird nur eine Zeile von der Datenbanktabelle gelesen. In welchem Datenobjekt steht die Information, welche Zeile gelesen werden soll?  
      Wann wird die Variable gefüllt, welche die Information über die zu lesende Zeile der Datenbanktabelle enthält?
