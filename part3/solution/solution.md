### 5. Untersuchen Sie die Anwendung:

1. Die Anwendung verwendet die elementare Variable `lv_customer_id` und die Strukturvariable `ls_customer`.
   Die Variablen wurden zu Beginn der Anwendung definiert.
2. Die Ausgabe auf der Konsole erfolgt über `out->write(…)`.
3. Über die Anweisung `SELECT` wird eine Datenbankabfrage realisiert.
   Die Daten werden von der Datenbanktabelle `/DMO/CUSTOMER` gelesen.
   Der Name der Datenbanktabelle wird in der `FROM`-Klausel der `SELECT`-Anweisung angegeben.
   Die Datenbanktabelle besitzt die Spalten `client`, `customer_id`, `first_name`, `last_name`…
4. Die Information über die gewünschte Zeile steht im Datenobjekt `lv_customer_id`.
   Dies kann der `WHERE`-Klausel der `SELECT`-Anweisung entnommen werden.
   Das Datenobjekt `lv_customer_id` wird in dieser Anwendung nach der Variablen-Definition gefüllt.
