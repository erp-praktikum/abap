# Kapitel 4: Datenbeschaffung - Füllen und sortieren einer internen Tabelle

Am Ende dieser Übungen können Sie:

- eine interne Tabelle durch Anhängen von Datensätzen füllen
- den Inhalt einer internen Tabelle sortieren

[Lösung](solution/solution.md)

Erweitern Sie Ihre ABAP-Anwendung zur Ausgabe der Reisen derart, dass die Reisen nach Tagen sortiert auf der Konsole erscheinen.  
Hängen Sie dazu die gelesenen Datensätze zum Kunden an eine interne Tabelle an

Kopiervorlage: `ZABAP010_##_SELECT_CUSTOMER`

Anwendung: `ZABAP010_##_SELECT_CUST_ITAB`

1. Kopieren Sie die ausführbare Anwendung `ZABAP010_##_SELECT_CUSTOMER` (Lösung vorherige Übung) auf den neuen Namen `ZABAP010_##_SELECT_CUST_ITAB`.

2. Definieren Sie eine interne Tabelle (Namensvorschlag: `lt_travel`) auf der Grundlage eines globalen Tabellentyps, der die globale Struktur `ZRAPH_##_TRAVEL_INFO` als Zeilentyp besitzt.  
   Hinweis: Nutzen Sie den Verwendungsnachweis, um solch einen geeigneten Tabellentyp zu finden.

3. Füllen Sie diese interne Tabelle zeilenweise mit Hilfe der `APPEND`-Anweisung innerhalb der `SELECT`-Schleife
   (Entfernen Sie die Anweisungen zur Konsolenausgabe; den Quelltext können Sie später wiederverwenden).

4. Sortieren Sie die interne Tabelle nach der Dauer in Tagen und geben Sie den Inhalt anschließend in einer `LOOP`-Schleife auf der Konsole aus.
