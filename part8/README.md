# Kapitel 4: Datenbeschaffung - Lesen mehrerer Zeilen per Schleife

Am Ende dieser Übungen können Sie:

- schleifenartige Datenbankzugriffe programmieren
- die Menge der selektierten Daten gemäß einem Eingabeparameter eingrenzen

[Lösung](solution/solution.md)

Erweitern Sie Ihre ABAP-Anwendung zur Ausgabe der Reisen derart, dass nur noch die Reisen für einen bestimmten Kunden ausgegeben werden.

Kopiervorlage: `ZABAP010_##_TR_LIST`

Anwendung: `ZABAP010_##_SELECT_CUSTOMER`

1. Kopieren Sie Ihre ausführbare Anwendung `ZABAP010_##_TR_LIST` (Lösung der Übung 2. zu Kapitel – Einführung in ABAP in Eclipse) auf den neuen Namen `ZABAP010_##_SELECT_CUSTOMER`.

2. Ändern Sie die Typisierung des Arbeitsbereichs (Namensvorschlag: `ls_travel`):  
   Verwenden Sie nun die globale Struktur `ZRAPH_##_TRAVEL_INFO`.  
   Informieren Sie sich über die Teilfelder dieser Struktur.  
   Diese Struktur enthält keine Komponenten `status`.  
   Entfernen Sie die Verwendung der Komponentne Status aus Ihrer Anwendung.

3. Definieren Sie eine Konstante für eine Kundennummer (Namensvorschlag: `lc_customer_id`).
   Setzen Sie die Konstante auf eine beliebige existierende Kundennummer.

4. Selektieren Sie nur die Reisen aus der Datenbanktabelle `/DMO/TRAVEL`, die zur vorgegebenen Kundenummer gehören.

5. Selektieren Sie nur noch diejenigen Felder, für die auch ein Zielfeld im Arbeitsbereich vorhanden ist.

6. Berechnen Sie innerhalb des Schleifenzugriffs für jede Reise die Dauer in Tagen.
   Weisen Sie das Ergebnis dem Feld `dayamount` Ihres Arbeitsbereiches zu.

7. Erweitern Sie die Konsolenausgabe um die Dauer in Tagen.  
   Hinweis: `CONCATENATE` unterstützt nur zeichenartige Variablen. `daymount` ist ein Integer, Sie müssen zuerst eine Umwandlung durchführen.
   Beispielsweise mit dem `CONV` Operator.

8. Warum ist die Anwendung mit dieser `WHERE`-Bedingung laufzeitgünstiger als die Anwendung bis Aufgabenteil 3?
