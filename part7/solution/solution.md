### 1. - 2.

Vorgehen wie in den Folien beschrieben.

### 3. Was wird ausgelöst, wenn kein Eintrag gefunden wurde?

Wenn kein Eintrag gefunden wurde, wird die Ausnahme `CX_SY_ITAB_LINE_NOT_FOUND` ausgelöst.

### 4. - 6. Musterlösung

Lösung inklusive 6. (Optional); nicht gezeigt: Nachrichtenklasse

    CLASS zabap010_itab_message DEFINITION
      PUBLIC
      FINAL
      CREATE PUBLIC .
    
      PUBLIC SECTION.
        INTERFACES if_oo_adt_classrun .
      PROTECTED SECTION.
      PRIVATE SECTION.
    ENDCLASS.
    
    
    
    CLASS zabap010_itab_message IMPLEMENTATION.
      METHOD if_oo_adt_classrun~main.
    
        CONSTANTS: lc_index TYPE i VALUE '5'.
    
        DATA lt_flight TYPE /dmo/t_flight.
        DATA ls_flight TYPE /dmo/flight.
    
        SELECT * FROM /dmo/flight INTO TABLE @lt_flight.
    
        TRY.
            ls_flight = lt_flight[ lc_index ].
    
            MESSAGE ID 'ZABAP010_##' TYPE 'I' NUMBER '001' WITH lc_index INTO DATA(lv_msg).
            out->write( EXPORTING data   = lv_msg ).
            out->write( EXPORTING data   = ls_flight ).
            out->write( EXPORTING data   = '----------------------------------------------------------------------------' ).
          CATCH cx_sy_itab_line_not_found.
            MESSAGE ID 'ZABAP010_##' TYPE 'I' NUMBER '002' WITH lc_index INTO lv_msg.
            out->write( EXPORTING data   = lv_msg ).
        ENDTRY.
    
    
        LOOP AT lt_flight INTO ls_flight.
          out->write( EXPORTING data   = ls_flight ).
        ENDLOOP.
    
      ENDMETHOD.
    
    ENDCLASS.
