# Kapitel 3: Grundlegende ABAP-Sprachelemente - Meldungen, Nachrichtenklassen und Nachrichten

Am Ende dieser Übungen können Sie:

- Eine Nachrichtenklasse anlegen
- Nachrichten anlegen und in der Anwendung nutzen

[Lösung](solution/solution.md)

Erweitern Sie Ihre Anwendung zu internen Tabellen.  
Es soll eine Zeile per Einzelsatz-Zugriff gelesen werden.  
Falls ein Satz gefunden wurde, soll eine Informations-Nachricht erscheinen.

Anwendung:	`ZABAP010_##_ITAB_LOOP`

1. Legen Sie eine Nachrichtenklasse mit dem Namen `ZABAP010_##` an.

2. Legen Sie eine Nachricht der Nummer `001` an, mit dem Text `Die interne Tabelle besitzt einen Eintrag in Zeile &1.`.  
Die Nachricht soll als selbsterklärend gekennzeichnet werden.

3. Suchen Sie die Schlüsselwort-Dokumentation zu Table Expressions zum Lesen einer Zeile einer internen Tabelle.  
Was wird ausgelöst, wenn kein Eintrag gefunden wurde?

4. Erweitern Sie Ihre Anwendung `ZABAP010_##_ITAB_LOOP`.  
   Lesen Sie nach der `SELECT`-Anweisung den fünften Eintrag der internen Tabelle.  
   Informieren Sie sich in der Schlüsselwort-Dokumentation zu Table Expressions über die Variante mit Indexzugriff.  
   Falls ein Satz gelesen wurde, soll die Nachricht `001` aus der Nachrichtenklasse `ZABAP010_##` mit dem Nachrichtentyp Information erscheinen.  
   Versorgen Sie den Platzhalter über den Zusatz `WITH` der `MESSAGE`-Anweisung mit der Zeilennummer.  
   Die Nachricht soll auf der Konsole ausgegeben werden, verwenden Sie den Zusatz `INTO` der `MESSAGE`-Anweisung und geben Sie eine Variable an, die den Nachrichtentext aufnimmt.  
   Diese kann dann auf der Konsole ausgegeben werden.  
   Geben Sie die Daten der gelesenen Zeile auf der Konsole aus.  
   Trennen Sie die Ausgabe dieser einzelnen Zeile von der Ausgabe der gesamten internen Tabelle durch eine beliebige Anzahl von Bindestrichen.  
   Testen Sie Ihre Anwendung.

5. Testen Sie die Anwendung im Debugging-Modus und prüfen sie, wie viele Zeilen die interne Tabelle nach dem Füllen enthält.  
   Ändern Sie die Anwendung so ab, dass der Einzelsatz-Zugriff in der internen Tabelle keinen Eintrag findet.
   Testen Sie Ihre Anwendung erneut. Die Nachricht sollte jetzt nicht erscheinen.

6. (Optional) Legen Sie eine Nachricht für die Situation an, dass bei einem Einzelsatz-Zugriff kein Eintrag gefunden wurde.  
   Erweitern Sie Ihre Anwendung so, dass diese Nachricht als Informations-Nachricht auf der Konsole erscheint, wenn das Laufzeitsystem keinen Satz gefunden hat.  
   Testen Sie die Anwendung.
