# Kapitel 3: Grundlegende ABAP-Sprachelemente - Arbeiten mit internen Tabellen

Am Ende dieser Übungen können Sie:

- im ABAP Dictionary nach passenden Tabellentypen suchen
- interne Tabellen auf der Grundlage eines globalen Tabellentyps definieren
- interne Tabellen per Array Fetch füllen
- den Inhalt von internen Tabellen per Schleife weiterverarbeiten

[Lösung](solution/solution.md)

Erstellen Sie eine ABAP-Anwendung, die Ihnen alle vorhandenen Flüge in der Konsole ausgibt.  
Beschaffen Sie sich die Daten aus der Datenbanktabelle `/DMO/FLIGHT`.  
Benutzen Sie anschließend die `LOOP ... ENDLOOP` Anweisung, um die einzelnen Datensätze in der Konsole auszugeben.

Anwendung: `ZABAP010_##_ITAB_LOOP`

1. Legen Sie die ausführbare Anwendung `ZABAP010_##_ITAB_LOOP` an.

2. In einer internen Tabelle sollen Daten der Datenbanktabelle `/DMO/FLIGHT` gepuffert werden.  
Es empfiehlt sich daher eine interne Tabelle so zu definieren, dass ihr Zeilentyp kompatibel zur Zeilenstruktur von `/DMO/FLIGHT` ist.  
Suchen Sie alle internen Tabellentypen heraus, die diese Bedingung erfüllen.  
Hinweis: Nutzen Sie den Verwendungsnachweis und die Filter Funktion.

3. Definieren Sie eine interne Tabelle (Namensvorschlag: `lt_flight`) auf der Grundlage eines der gefundenen globalen Tabellentypen.

4. Definieren Sie einen zur internen Tabelle passenden Arbeitsbereich (Namensvorschlag: `ls_flight`).

5. Programmieren Sie einen Array-Fetch-Zugriff auf alle Datensätze der Datenbanktabelle `/DMO/FLIGHT`:

        SELECT * FROM /dmo/flight
        INTO TABLE @lt_flight.

6. Geben Sie die gepufferten Daten auf der Konsole aus.  
Hinweis: Verwenden Sie dazu die `LOOP`-Anweisung.
