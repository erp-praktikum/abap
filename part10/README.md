# Kapitel 4: Datenbeschaffung - SQL Konsole und Join

Am Ende dieser Übungen können Sie:

- eine Abfrage auf der SQL Konsole ausführen

[Lösung](solution/solution.md)

1. Starten Sie die SQL Konsole mit Rechts-Klick auf das System in Eclipse.

2. Formulieren Sie eine SQL Abfrage die die folgenden Felder der Tabelle für Reisen (`/DMO/TRAVEL`) ausgeben soll:  
   `TRAVEL_ID`  
   `CUSTOMER_ID`  
   Führen Sie die Abfrage aus.

3. Erweitern Sie die Abfrage, dass zusätzlich zur Kundennummer (`CUSTOMER_ID`) noch der Nachname aus der Tabelle für Kunden (`/DMO/CUSTOMER`) ausgegeben wird.  
   Führen Sie die Abfrage aus.  
   Hinweis: Verwendung Sie einen `JOIN`.
