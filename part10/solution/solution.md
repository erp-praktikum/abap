### Musterlösung bis 2.

    SELECT travel_id, customer_id
      FROM /dmo/travel

### Musterlösung:

    SELECT
      travel~travel_id,
      travel~customer_id,
      customer~last_name
      FROM /dmo/travel AS travel
      JOIN /dmo/customer AS customer ON customer~customer_id = travel~customer_id
