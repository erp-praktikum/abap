# Kapitel 1: Einführung in ABAP in Eclipse - Entwickeln von ABAP-Anwendungen

Am Ende dieser Übungen können Sie:

- ABAP-Anwendungen anlegen; editieren; sichern und weiterbearbeiten
- ABAP-Anwendungen auf ihre syntaktische Korrektheit überprüfen
- ABAP-Anwendungen testen und aktivieren

[Lösung](solution/solution.md)

Erstellen Sie eine ABAP-Anwendung, die Ihnen Zusatzinformationen zu allen vorhandenen Reisen auf der Konsole ausgibt.  
Beschaffen Sie sich die Daten aus der Datenbanktabelle `/DMO/TRAVEL`.  
Anwendung: `ZABAP010_##_TR_LIST` (## steht für die zweistellige Gruppennummer)

1. Legen Sie die Klasse `ZABAP010_##_TR_LIST` für eine Konsolenanwendung an (##: Benutzernummer oder beachten Sie die Namenskonventionen, die Ihnen der Dozent vorgibt).  
Tragen Sie im Popup for die ABAP Klasse bei Interfaces if_oo_adt_classrun ein.

Fügen Sie im folgenden die Anweisungen zwischen "METHOD if_oo_adt_classrun~main. ... ENDMETHOD." ein.
        
2. Definieren Sie einen Arbeitsbereich für die Datenbeschaffung (Namensvorschlag: ls_travel) und ein Datenobjekt für die Ausgabe (Namensvorschlag: lv_concat).

        DATA ls_travel TYPE /dmo/travel.
        DATA lv_concat TYPE string.

3. Programmieren Sie einen Schleifenzugriff auf alle Datensätze der Datenbanktabelle `/DMO/TRAVEL`.

        SELECT * FROM /dmo/travel
        INTO CORRESPONDING FIELDS OF @ls_travel.
        ...
        ENDSELECT.

4. Geben Sie für jeden Datensatz die Inhalte der Felder `travel_id`, `agency_id`, `customer_id` und `status` auf der Konsole aus:

        CONCATENATE ls_travel-travel_id
          ls_travel-agency_id
          …
        INTO lv_concat SEPARATED BY space.
        out->write( EXPORTING data = lv_concat ).

5. Prüfen Sie Ihre Anwendung auf Syntaxfehler, aktivieren und testen Sie sie.
