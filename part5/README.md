# Kapitel 3: Grundlegende ABAP-Sprachelemente - Arbeiten mit Strukturen

Am Ende dieser Übungen können Sie:

- den Debugger einsetzen, um den Datenfluss zu verfolgen
- die ABAP-Anweisung `MOVE-CORRESPONDING` einsetzen, um Wertzuweisungen zwischen Strukturen vorzunehmen

[Lösung](solution/solution.md)

Eine gegebene ABAP-Anwendung soll dahingehend untersucht werden, wann und wie welche Daten zwischen welchen strukturierten Datenobjekten transportiert werden.  
Anwendung: `ZABAP010_##_GET_START_2`

1. Legen Sie die Klasse `ZABAP010_##_GET_START_2` für eine Konsolenanwendung an (##: Benutzernummer oder beachten Sie die Namenskonventionen, die Ihnen der Dozent vorgibt).

2. Fügen Sie folgenden Quelltext für die Anwendung ein:

        TYPES:
          BEGIN OF lst_travel_info,
            travel_id   TYPE /dmo/travel_id,
            agency_id   TYPE /dmo/agency_id,
            customer_id TYPE /dmo/customer_id,
            begin_date  TYPE /dmo/begin_date,
            end_date    TYPE /dmo/end_date,
            dayamount   TYPE i,
          END OF lst_travel_info .
        
        DATA lv_travel_id TYPE /dmo/travel_id.
        DATA ls_travel TYPE /dmo/travel.
        DATA ls_travel_info TYPE lst_travel_info.
        
        lv_travel_id = '00000007'.
        
        SELECT SINGLE * FROM /dmo/travel
         WHERE travel_id = @lv_travel_id
         INTO @ls_travel.
        
        MOVE-CORRESPONDING ls_travel TO ls_travel_info.

3. Aktivieren Sie die Anwendung.

4. Setzen Sie einen Breakpoint bei der ersten Anweisung.

5. Führen Sie die Anwendung aus.

6. Überzeugen Sie sich davon, ob alle Datenobjekte initialisiert sind.  
   Übernehmen Sie alle in der Anwendung definierten Datenobjekte in die Feldansicht auf.  
   Informieren Sie sich über die Struktur und die Typisierung der einzelnen Komponenten.

7. Verfolgen Sie den Anwendungsfluss, indem Sie mit Einzelschritt (F5) immer zur nächsten ABAP-Anweisung springen.  
   Welche Felder der Struktur `ls_travel` werden durch die `SELECT`-Anweisung gefüllt?  
   Welchen Wert hat das Systemfeld `sy-subrc` nach dieser Anweisung?

8. Verfolgen Sie den Kopiervorgang von der Struktur `ls_travel` in die Struktur `ls_travel_info`.  
   Welche Feldwerte werden kopiert?
