### 7. Verfolgen des Anwendungsflusses
Welche Komponenten werden durch die SELECT-Anweisung gefüllt?  
Alle Felder der Tabelle `/DMO/TRAVEL`.

Welchen Wert hat das Systemfeld sy-subrc nach der SELECT-Anweisung?  
Da in der Datenbanktabelle `/DMO/TRAVEL` ein Datensatz für die Reise mit der Nummer `00000007` vorhanden ist, wird `sy-subrc` auf 0 gesetzt.

### 8. Verfolgen des Kopiervorganges
Welche Feldwerte werden kopiert?  
`travel_id`, `agency_id`, `customer_id`, `begin_date`, `end_date`
