# Exercises ABAP


## Inhaltsverzeichnis

### Kapitel 1: Einführung in ABAP in Eclipse

#### [Paket anlegen](part1/README.md)
#### [Entwickeln von ABAP-Anwendungen](part2/README.md)

### Kapitel 2: Einführung in das ABAP Dictionary

#### [Verwendung von globalen Strukturen für Datenobjekte](part3/README.md)

### Kapitel 3: Grundlegende ABAP Sprachelemente

#### [Grundlegende ABAP-Anweisungen](part4/README.md)
#### [Arbeiten mit Strukturen](part5/README.md)
#### [Arbeiten mit internen Tabellen](part6/README.md)
#### [Meldungen, Nachrichtenklassen und Nachrichten](part7/README.md)

### Kapitel 4: Datenbeschaffung

#### [Lesen mehrerer Zeilen per Schleife](part8/README.md)
#### [Füllen und Sortieren einer internen Tabelle](part9/README.md)
#### [SQL Konsole und Join](part10/README.md)
#### [Ändernde Zugriffe](part11/README.md)
