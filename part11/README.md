# Kapitel 4: Datenbeschaffung - Ändernde Zugriffe

Am Ende dieser Übungen können Sie:

- Datenbanktabellen leeren und Einträge einfügen

[Lösung](solution/solution.md)

Sie haben bereits eigene Datenbanktabellen angelegt. Diese enthalten noch keine Daten.  
Fügen Sie Daten in die Datenbanktabellen ein.

Anwendung:	`ZABAP010_##_DATA_GENERATOR`

1. Legen Sie die ausführbare Anwendung `ZABAP010_##_DATA_GENERATOR` an.

2. Die Anwendung soll immer wieder aufgerufen werden können.  
   Also müssen Sie zu Beginn dafür sorgen, dass die bereits vorhandenen Einträge in den Datenbanktabellen `ZRAPH_##_Travel`, `ZRAPH_##_Booking`, `ZRAPH_##_BookSup` und `ZRAPH_##_RoomRsv` gelöscht werden.  
   Verwenden Sie dazu die `DELETE` Anweisung auf die Datenbanktabellen.

3. Füllen Sie Ihre persönlichen Travel-, Booking- und BookingSupplement-Tabellen mit den Daten aus den entsprechenden `/DMO/`-Referenz-Tabellen (`/DMO/A_TRAVEL_D`, `/DMO/A_BOOKING_D` und `/DMO/A_BKSUPPL_D`).  
   Hierzu müssen sie zuerst die Einträge aus einer der `/DMO/`-Referenz-Tabellen selektieren und dann mit der `INSERT` Anweisung in Ihre zugehörige persönlichen Datenbanktabellen einfügen.

4. Für die Raumreservierungen müssen Einträge basierend auf den jeweils verfügbaren Hotels und Reisen dynamisch generiert werden.  
   Verwenden Sie dazu den folgenden Quellcode:

        " Clear personal room reservation table and fill it from data generated
        " based on existing travels and hotels
        DATA roomreservations TYPE STANDARD TABLE OF zraph_##_roomrsv WITH DEFAULT KEY.
        SELECT COUNT( * ) FROM zraph_hotel INTO @DATA(hotel_count).
        IF hotel_count = 0.
          out->write( 'Aborted: No hotels found!' ).
          RETURN.
        ENDIF.
        DELETE FROM zraph_##_roomrsv.
    
        SELECT travel_uuid, begin_date, end_date, total_price, currency_code
          FROM zraph_##_travel INTO TABLE @DATA(travels).
        SELECT hotel_id FROM zraph_hotel INTO TABLE @DATA(hotels).
        LOOP AT travels ASSIGNING FIELD-SYMBOL(<travel>).
          TRY.
            DATA(index) = sy-tabix.
            READ TABLE hotels INDEX index MOD hotel_count + 1 INTO DATA(hotel).
            IF index MOD 4 <= 2.
              APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                              roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                              roomrsv_id    = '000001'
                              hotel_id      = hotel-hotel_id
                              begin_date    = <travel>-begin_date
                              end_date      = <travel>-end_date
                              room_type     = 'S'
                              roomrsv_price = <travel>-total_price * '0.15'
                              currency_code = <travel>-currency_code ) TO roomreservations.
              IF index MOD 4 = 1.
                APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                                roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                                roomrsv_id    = '000002'
                                hotel_id      = hotel-hotel_id
                                begin_date    = <travel>-begin_date
                                end_date      = <travel>-end_date
                                room_type     = 'D'
                                roomrsv_price = <travel>-total_price * '0.25'
                                currency_code = <travel>-currency_code ) TO roomreservations.
              ELSEIF index MOD 4 = 2.
                APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                                roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                                roomrsv_id    = '000002'
                                hotel_id      = hotel-hotel_id
                                begin_date    = <travel>-begin_date
                                end_date      = <travel>-end_date
                                room_type     = 'F'
                                roomrsv_price = <travel>-total_price * '0.4'
                                currency_code = <travel>-currency_code ) TO roomreservations.
              ENDIF.
            ENDIF.
            IF index MOD 4 = 3.
              APPEND VALUE #( parent_uuid   = <travel>-travel_uuid
                              roomrsv_uuid  = cl_system_uuid=>create_uuid_x16_static( )
                              roomrsv_id    = '000001'
                              hotel_id      = hotel-hotel_id
                              begin_date    = <travel>-begin_date
                              end_date      = <travel>-end_date
                              room_type     = 'E'
                              roomrsv_price = <travel>-total_price * '0.7'
                              currency_code = <travel>-currency_code ) TO roomreservations.
            ENDIF.
            CATCH cx_uuid_error.
          ENDTY.
        ENDLOOP.
        INSERT zraph_##_roomrsv FROM TABLE @roomreservations.
        out->write( 'Room reservation data generated.' ).


5. Führen Sie die Anwendung aus und prüfen Sie mit der Datenvorschau, dass anschließend Werte in die Datenbanktabellen `ZRAPH_##_Travel`, `ZRAPH_##_Booking`, `ZRAPH_##_BookSup` und `ZRAPH_##_RoomRsv` eingefügt wurden.
