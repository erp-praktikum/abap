# Kapitel 3: Grundlegende ABAP-Sprachelemente - Grundlegende ABAP-Anweisungen

Am Ende dieser Übungen können Sie:

- elementare Datenobjekte definieren
- Werte zuweisen
- bedingte Verzweigungen implementieren
- Berechnungen durchführen

[Lösung](solution/solution.md)

Es soll eine einfache ABAP-Anwendung für die vier Grundrechenarten erstellt werden.  
Die Werte und der Rechenoperator werden in der Anwendung vorgegeben. Das Ergebnis soll auf der Konsole ausgegeben werden.  
Anwendung:	`ZABAP010_##_COMPUTE` (## steht für die zweistellige Gruppennummer)

1. Legen Sie die ausführbare Anwendung `ZABAP010_##_COMPUTE` an.

2. Definieren Sie Variablen für zwei ganze Zahlen (Namensvorschlag: `lv_int1`, `lv_int2`) und einen Rechenoperator (Namensvorschlag: `lv_op`).  
   Definieren Sie ferner ein elementares Datenobjekt für das Ergebnis vom Typ gepackte Zahl mit zwei Nachkommastellen (Namensvorschlag: `lv_result`).

3. Führen Sie die Berechnung des Ergebnisses in Abhängigkeit vom eingegebenen Rechenoperator durch.  
   Hinweis: Verwenden Sie dazu die CASE-Anweisung.

4. Geben Sie das Ergebnis auf der Konsole aus.

5. Geben Sie eine Fehlermeldung auf der Konsole aus, falls der Anwender einen ungültigen Rechenoperator eingegeben hat.  
   Hinweis: Verwenden Sie dazu die IF-Anweisung.

6. Geben Sie eine Fehlermeldung auf der Konsole aus, falls der Anwender durch null dividieren will.

7. Testen Sie Ihre Anwendung.  
Leider sind Eingaben bei Konsolenanwendungen aktuell nicht möglich.  
Sie können Eingaben simulieren, indem Sie die Variablen lv_int1, lv_int2 und lv_op zu Beginn der Anwendung Werte zuweisen.
