# Kapitel 1: Einführung in ABAP in Eclipse - Paket anlegn


Am Ende dieser Übungen können Sie:

- ABAP-Paket anlegen; editieren; sichern

Erstellen Sie ein ABAP-Paket, in welchem alle Anwendungen dieses Kurses angelegt werden sollen.
Paket: `ZABAP010_##` (## steht für die zweistellige Benutzernummer)

1. Legen Sie das Paket `ZABAP010_##` an (##: Benutzernummer oder beachten Sie die Namenskonventionen, die Ihnen der Dozent vorgibt).
2. Falls noch nicht vorhanden legen Sie sich einen Transport an.
